GNU Lesser General Public License (LGPL) version 2

This is typchecking of typed scheme for guile

  Copyright (c) Stefan Israelsson Tampe.

See "COPYING.LIB" for more information about the license.
