(define-module (type typed-guile example  )
  #:use-module (type typed-guile          )
  #:use-module (type typed-guile register )
  #:export (run1))

(set! *type-trace* #f)
#|
Type system setup
|#
(set! *type-symbols* 
      '(lambda arg 
         or and not
         cons tree1 tree2 list list2 nil 
         rec up down void
         string symbol char boolean
         number integer))

(set! *predtypes*
     `((string?  ,(lambda () 'string  ))
       (symbol?  ,(lambda () 'symbol  ))
       (integer? ,(lambda () 'integer ))
       (number?  ,(lambda () 'number  ))
       (boolean? ,(lambda () 'boolean ))
       (pair?    ,(lambda () `(cons ,(gensym "A")  ,(gensym "B"))))
       (char?    ,(lambda () 'char    ))))

(declare +     : (lambda (number number) number))
(declare -     : (lambda (number number) number))
(declare <     : (lambda (number number) boolean))

(declare cons  : (lambda (A B) (cons A B)))
(declare car   : (lambda ((cons A B)) A))
(declare cdr   : (lambda ((cons A B)) B))
(declare pair? : (lambda (A) boolean))


(subtype integer  is-a  number)

;;note that we need to keep parts of the or term disjuctive
;;otherwize the solver complecity will explode!!

(define-recursive-type (tree1 A)    
  (or (and A (not (cons B C)))  
      (cons (tree1 A) (tree1 A))))


;; An alternative because A => (cons (tree A) (tree A)) 
;; we can use the alternative 
(define-recursive-type (tree2 A)    
  (or (cons (tree2 A) (tree2 A))
      A))


(define-recursive-type (list A)    
  (or nil (cons A (list A))))

(define-recursive-type (list2 A B) 
  (or (cons A (list2 A B))
      B))
           


#|
subtype examples
|#

(pk 1)
#;
(tdefine (sub1 X)
  (type (lambda (integer) number)
    (+ X X)))


#| 
recursive examples
|#

(pk 2)
(tdefine (rec1 X)
  (type (lambda ((tree1 char))  (tree1 char))
    (if (pair? X)
        (let ((Car (car X)))
          (if (pair? Car)
              (car Car)
              Car))
        X)))


(pk 3)
(tdefine (rec2 X)
  (type (lambda ((tree1 A)) (tree1 A))
    (if (pair? X)
        (let ((Car (car X)))
          (if (pair? Car)
              (car Car)
              Car))
        X)))

(pk 4)
(tdefine (rec2 X)
  (type (lambda ((tree2 A)) (tree2 A))
    (if (pair? X)
        (let ((Car (car X)))
          (if (pair? Car)
              (car Car)
              Car))
        X)))

(pk 5)
(tdefine (rec3 X)
  (type (lambda ((tree2 A)) (tree2 A))
    (if (pair? X)
        (let ((Car (car X)))
          (if (pair? Car)
              (rec2 (car Car))
              (rec2 Car)))
        (rec2 X))))
  
;; Trying to compare infinite sequence types

(define-recursive-type (up A B)
  (or (cons A (up A B))
      (cons B (up A B))))

(define-recursive-type (down A B)
  (cons A (cons A (down B A))))
           

;; this will typecheck without a diving into an infinite loop
(pk 6)
(tdefine (down/up1 X)
  (type (lambda ((down char number)) (up char number))
    X))


(tdefine (down/up2 X)
  (type (lambda ((down A B)) (up A B))
    X))

(catch 
  #t 
  (lambda ()  
    (tdefine (down/up3 X)
      (type (lambda ((down A B)) (up A A))
        X)))
  (lambda x 
    (format #t "OK, if this is a type error~%")))

(tdefine  a-car
  (case-lambda
    ((A)   (type (lambda ((list number)) number) 
             (if (pair? A)
                 (car A)
                 0)))
    ((A B) (type (lambda ((list number) number) number)
             (if (pair? A)
                 (car A)
                 B)))))

(tdefine (car10 X)
  (type (lambda ((list number)) number)
    (a-car X 10)))

(tdefine (my-map F L)
  (type (lambda ((lambda (arg A) B) (list A)) (list B))
    (letrec ((H (lambda (L)
                  (type (lambda ((list A)) (list B))
                    (if (pair? L)
                        (cons (F (car L)) (H (cdr L)))
                        '())))))
      (H L))))

(tdefine (double-it L)
  (type (lambda ((list number)) (list number))
    (my-map (lambda (X)
              (type (lambda (number) number)
                (+ X X)))
            L)))

(tdefine (oddeven N)
  (type (lambda (number) number)
    (letrec ((Odd   (lambda (N M) 
                      (type (lambda (number number) number)
                        (Even (- N 1) M))))
             (Even  (lambda (N M) 
                      (type (lambda (number number) number)
                        (if (< N 0) 
                            M
                            (Odd (- N 1) (+ M 1)))))))
      (Even N 0))))


;buggy and by design set! sematics are weak but it's possible
;to use multiple types although it's a fragil construction so 
;try to avoid using this and keep a single type for a variable
(tdefine (myset X Y)
  (type (lambda (number symbol) symbol)
    (let ((A (type (or number symbol)
               1)))
      (set! A (+ X 1))
      (set! A Y)
      'ok)))

