(define-module  (type typed-guile register)
  #:use-module (ice-9 match)
  #:export     (register-fkn-signature get-fkn-sign *type-trace*
                *type-symbols* homogenize-type-symbols
                *predtypes*))

(define *type-trace* #f)

;; *********** FUNCTION SIGNATURE DATABASE ***********************
(define *fkn-type-map* (make-weak-key-hash-table 1000))
(define (get-fkn-sign m s)
  (let ((f (module-symbol-binding m s)))
    (if (procedure? f)
        (let ((ret (hash-ref *fkn-type-map* f)))
          (if ret 
              (homogenize-type-symbols ret)
              (error (format #f "function ~a has no attached signature" s))))
        (error (format #f 
                       "tries to find the signature to non function object ~a"
                       s)))))

(define (register-fkn-signature s f v)
  (hash-set! *fkn-type-map* f 
             (match v
               (('lambda (x ...) r) `(lambda (arg ,@x) ,r))
               (x x)))
  (format #t "declared ~a : ~a~%-----------------------------------~%" s v))  
     
;;--------------------------------------------------------------
;; **************************** type system symbols ************
(define (solve-var? x)
  (and (symbol? x)
       (char-upper-case? (car (string->list (symbol->string x))))))

(define *type-symbols* '())

(define (get-sym x)
  (let loop ((l *type-symbols*))
    (match l
      ((y . l)  
       (if (equal? (symbol->string y) (symbol->string x))
           y
           (loop l)))
      (() (error (format #f "~a is not a symbol in the type system" x))))))

(define (homogenize-type-symbols x)
  (let loop ((x x))
    (match x
      ((x . l) 
       (cons (loop x) (loop l)))      
      ((? solve-var? x) x)
      ((? symbol?    x) (get-sym x))
      (x                x))))

;; ------------------------------------------------------------------
;; ****************************** PREDTYPES ************************
(define *predtypes* '())
