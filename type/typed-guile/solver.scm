(define-module (type typed-guile solver        )
  #:use-module (type typed-guile equationalize )
  #:use-module (type typed-guile register      )
  #:use-module (logic  guile-log               )
  #:use-module (logic  guile-log umatch        )
  #:use-module (srfi     srfi-1                )
  #:use-module (srfi     srfi-11               )
  #:use-module (ice-9    pretty-print          )  
  #:use-module (ice-9    match                 )  
  #:use-module (ice-9    receive               )
  #:duplicates (first)
  #:export (type-solver code->solver 
                        add-subtype-relation=> 
                        add-subtype-relation<=> 
                        define-parameter-type
                        ))

;;------------------------------------------------------------------------------
(define (gp-cp x)
  (umatch (#:mode - #:status *S*) (x)
    ((x . l) (gp-cons! (gp-cp x) (gp-cp l) *S*))
    (x       (gp-lookup x *S*))))

;;Bank interface
(define *S*            *current-stack*)
(define *bank-a*       *current-stack*)
(define *bank-b*       (gp-make-stack 1 0 10000 10000 10000))

(gp-thread-safe-set! *bank-b* #f)

(define-syntax-rule (u-abort f)  (f))
(define-syntax-rule (u-set! s v) (gp-set! s v *S*))
(define-syntax-rule (u-scm x)    (gp->scm x *S*))
(define-syntax-rule (u-var!)     (gp-var! *S*))
(define-syntax-rule (u-cons x y) (gp-cons! x y *S*))
(define-syntax-rule (u-dynwind x y z)
  (gp-dynwind x y z *S*))

(define (gp-swap-to-a) (set! *S* *bank-a*))
(define (gp-swap-to-b) (set! *S* *bank-b*))



#|
Type solver utility, 
and - types
or  - types
not - types
subtypes
parameter types
recursive types
|#

;;

(define old-solve-var? solve-var?)
(define (solve-var? x) (old-solve-var? (gp-lookup x *S*)))

(define *res-vars* '())
(define var? (lambda (x) (gp-var? (gp-lookup x *S*) *S*)))

;; ******************************** TRACE FUNCTIONALITY ******************
(define (pr . L)
  (define (s M)
    (let loop ((S "") (M M))
      (if (= M 0)
          S
          (loop (string-append " " S) (- M 1)))))
  (if *type-trace*
      (match (my-scm L *S*)
        ((M N X Y)  
         (begin
           (format #t "~a~10a  ~1a ~1a~%~a~a~%~a~a~%~%" 
                   (s M) N "" "" (s M) X (s M) Y)))
        ((M N E X Y)  
         (begin
           (format #t "~a~10a  ~1a ~1a~%~a~a~%~a~a~%~%" 
                   (s M) N "" E (s M) X (s M) Y)))
        ((M N Ex Ey X Y)  
         (begin
           (format #t "~a~10a  ~1a ~1a~%~a~a~%~a~a~%~%" 
                   (s M) N Ex Ey (s M) X (s M) Y))))))
;-----------------------------------------------------------------------

;; => points in the direction of a more general type
(define *sub-type-relation=>* '()) 
(define *sub-type-relation<=* '()) 

(define (homogenize x)
  (let* ((r (gp-newframe *S*))
         (ret (my-scm (sym->var x) *S*)))
    (gp-unwind r)
    ret))
    
(define (?? x) (lambda (y) (equal? x y)))

(define (fasr a b li)
  (match (homogenize
          (match (cons a b)
            (((a . args) . b)   `(,a ,args ,b))
            ((a          . b)   `(,a #f    ,b))))
    ((a arg b)
     (let loop ((x li))
       (match x
         ((((? (?? a)) (? (?? arg)) . l) . u)
          `((,a ,arg ,b ,@l) ,@u))
         (( x       . u)
          (cons x (loop u)))
         (()
          (list (list a arg b))))))))

(define (add-subtype-relation=> a b)
  (format #t "(~a) is a (~a)~%" a b)
  (set!  *sub-type-relation=>* (fasr a b *sub-type-relation=>*))
  (set!  *sub-type-relation<=* (fasr b a *sub-type-relation<=*)))

(define (add-subtype-relation<=> a b)
  (format #t "(~a) alias (~a)~%" a b)
  (add-subtype-relation=> a b)
  (add-subtype-relation=> b a))

(define (macro-relation-> a b)  
  (format #t "(~a) transform into (~a)~%" a b)
  (set!  *sub-type-relation=>* (fasr a b *sub-type-relation=>*))
  (set!  *sub-type-relation<=* (fasr a b *sub-type-relation<=*)))

(define-syntax define-parameter-type
  (syntax-rules ()
    ((_ n v)  (macro-relation-> 'n 'v))))

(define (prepare-subtype)
  (define (clean x s)
    (match x
      (((y . l) . u) 
       (set-symbol-property! y s '())
       (clean u s))
      (() 'ok)))
  
  (define (add x s)
    (match x
      (((x . l) . u)
       (set-symbol-property! x s (sym->var l))
       (add u s))
      (() '())))

  (clean *sub-type-relation=>* '=>)
  (clean *sub-type-relation<=* '<=)
  (add   *sub-type-relation=>* '=>)
  (add   *sub-type-relation<=* '<=))
       
       
(<define> (<forall> Z L N Id Cut)
  ;;(<code> (pp (my-scm `(forall ,N ,Id : ,L) S)))
  (<match> (#:mode -) (L)
    ((A . B)  
     (<let> ((Fr (b-frame)))
       (b-page Fr
               (</.>
                   (<cut> 
                    (<or> 
                     (<=> A Z)
                     (<and> 
                      (<when> (begin (b-unwind Fr) #t)
                              (<forall> Z B (+ N 1) Id Cut)))))) 0)))
    (_        
     (<fail> Cut))))


(define (sub* q)
  (lambda (S Pr CC X Z Cut)
    (call-with-prompt 'leave
      (lambda ()
        (define (f)
          (umatch (#:mode - #:status *S*) (X)
            ((? var?)  (abort-to-prompt 'leave #f))
            ((X . A)   (cons X  A))
            (X         (cons X #f))))

        (let* ((x.A (f))
               (x   (car x.A))
               (a   (cdr x.A))
               (F   (lambda (G l L)
                      (umatch (#:mode + #:status *S*) ((sym->var l))
                        ((,a . U)
                         (add-var-points U))
                        (_ 
                         (G L)))))
               
               (l   (let loop  ((l (q)))
                      (match l
                        ((X . L)
                         (umatch (#:mode - #:status *S*) (X)
                           ((,x . l)
                            (F loop l L))
                           (_ (loop L))))
                        
                        ((_ . L)  
                         (loop L))
                        
                        (()                 
                         (abort-to-prompt 'leave #f))))))
          (abort-to-prompt 'leave 
                          (lambda ()
                            (<forall> S Pr CC Z l 0 (gensym "id") Cut)))))
      (lambda (c x) (if x (x) (Pr))))))

(define (s-t-=>) *sub-type-relation=>*)
(define (s-t-<=) *sub-type-relation<=*)

(define sub=> (sub* s-t-=>))
(define sub<= (sub* s-t-<=))


(<define> (<subtype-unify-ar> F Ex Ey X Y)
    (<match> (#:mode -) (X Y)
             (('or . L) _  (<cut> (F Ex Ey X Y)))
             (_ ('and . L) (<cut> (F Ex Ey X Y)))
             (_ _          (<cut> (<subtype-unify-ar0> F Ex Ey X Y)))))

(<define> (<subtype-unify-ar0> F Ex Ey X Y)
  (<dyn> (pr 6 'subtype X Y) (pr 6 'leave-subtype X Y))
  (<var> (Z)
    (<peek-fail> Cut
       ;; If sub<= succeeds then a Cut will skip     
       ;; the last part in the <or> below
       (<or> (F Ex Ey X Y)        
             (<and> 
              (sub<= X Z Cut)
              (<when> (begin 
                        (pr 6 'subtype<= Z Y)
                        (if (gp-fluid-ref *validated*) 
                            (u-set! X Z)) 
                        #t))
              (unify-rec
               (<lambda> (x y)
                 (<subtype-unify-ar> 
                  %a=r% Ex Ey x y))
               Z 
               Y))

             (<and> 
              (sub=> Y Z Cut) 
              (<when> (begin 
                        (pr 6 'subtype=> X Z)
                        (if (gp-fluid-ref *validated*) 
                            (u-set! Y Z)) 
                        #t))
              (unify-rec
               (<lambda> (x y)
                 (<subtype-unify-ar> 
                  %a=r% Ex Ey x y))
               X
               Z))))))


;; ***** printing utility ******
(define (my-scm X *S*)
  (define *A* '(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z))
  (define *vars* '())
  (define *syms* '())
    
  (define (get-var X V S)   
    (define (f? Y) (eq? (gp-lookup X *S*) (gp-lookup Y *S*)))

    ;;(pp 'get-var)
    (umatch (#:mode - #:status *S*) (V)
      (()            
       (begin
         (if (null? *A*) 
             (set! *A* (map (lambda x (gensym "A")) 
                            '(1 1 1 1 1 1 1 1 1))))
         (set! *vars* (cons X *vars*))
         (set! *syms* (cons (car *A*) *syms*))
         (set! *A*    (cdr *A*))
         (car *syms*)))
      ((Y  . L)       
       (if (f? Y)
           (car S)
           (get-var X L (cdr S))))))

  (define (f X)
    ;;(pp `(get-var.f ,X))
    (umatch (#:mode - #:status *S*) (X)
      ((X . L)       
       (cons (f X) (f L)))
      ((? var? X) 
       (get-var X *vars* *syms*))
      (X             
       (u-scm X))))
  (f X))


;; ************************* B-bank helpers **************
(define *u1* #f)
(define *u2* #f)

(define (swap-u1-u2 S Pr CC)
  (gp-dynwind
   (lambda () #t)
   (lambda () 
     (let ((r *u2*))
       (set! *u2* *u1*)
       (set! *u1* r)
       (CC S Pr)))
   (lambda x
     (let ((r *u2*))
        (set! *u2* *u1*)
        (set! *u1* r)))
   S))

(define (b-frame)    (map (lambda (x) x) (gp-newframe *bank-b*)))
(define (b-unwind K) (when (car K)
                       (gp-unwind K)
                       (set-car! K #f)))
(define (b-cons X Y) (gp-cons! X Y *bank-b*))
(define (b-page S Pr CC Fr F . l)
  (gp-dynwind
   (lambda x #t)
   (lambda ()
     (gp-dynwind
      (lambda x  #t)
      (lambda () (F S Pr CC))
      (lambda x  (b-unwind Fr))
      S))
   (lambda x (set-car! Fr #f))
   *bank-b*))


(define (b-page* S Pr CC Fr F . l)
  (F S Pr CC))

;****************************************************************''
;; *********** Recursive datatypes *****************

(define (sync-budy-data Cold Cnew)
  (define (sync-budy-data-arg Lx Ly)
    (umatch (#:mode - #:status *S*) (Lx Ly)
      ((X . Lx) (Y . Ly)
       (begin
         (f X Y)
         (sync-budy-data-arg Lx Ly)))
      (() () #t)))

  (define (f Cold Cnew)
    (umatch (#:mode - #:status *S*) (Cold Cnew)
      ((X . Lx) (Y . Ly)
       (let ((Bx (gp-budy Cold))
             (By (gp-budy Cnew)))
         (gp-set! By Bx *S*)
         (sync-budy-data-arg Lx Ly)))
            
      (()       ()
       #t)
            
      (_ _
         (let ((Bx (gp-budy Cold))
               (By (gp-budy Cnew)))
           (gp-set! By Bx *S*)
           #t))))

  (gp-swap-to-b)
  (f Cold Cnew)
  (gp-swap-to-a))

     
#|
There is a special matcher to find rec patterns
(rec r (A) (or (f1 A) (f2 A)))
(rec r (a) (or (f2 a) (f2 a)))
|#


(define (rec-match S Pat X)
  (define vars *res-vars*)
  (define (f Pat X)
    ;(pp (my-scm `(match ,Pat ,X)))
    (umatch (#:mode - #:status S) (Pat X)
      ((X . Lx)   (Y . Ly)
       (and (f X Y) (f Lx Ly)))

      ((? var? X) (? var? Y)
       (let ((X (gp-lookup X S))
             (Y (gp-lookup Y S)))
         (if (member Y vars)
             (eq? X Y)
             (begin
               (if (not (eq? Y X))
                   (gp-set! Y X S))
               (set! vars (cons X vars))
               #t))))
      (X X #t)
      (_ _ #f)))

  (let* ((Fr  (gp-newframe S))
         (Ret (f Pat X)))
    (gp-unwind Fr)
    Ret))
   
                  
(define (equal-match S X Y)
  (umatch (#:mode - #:status S) (X Y)
    ((X . Lx) (Y . Ly)
     (and (equal-match S X  Y)
          (equal-match S Lx Ly)))
    (X X #t)
    (_ _ #f)))

(define (add-rec S rec A C L CC)  
  (define (f l)
    ;(pp (my-scm `(add-rec/f ,A ,C ,l) S))
    (umatch (#:mode - #:status S) (l)
      (((R Ar Cr) . l)
       (begin
         (if (equal-match S `(,R ,Ar) `(,rec ,A))
             (begin
               (sync-budy-data Cr C)
               (CC S C))
             (begin
               (f l)))))
      (V 
       (begin
         (gp-swap-to-b)
         (let ((Y (add-var-points 
                   (gp-cp 
                    (list rec A C)))))
           (u-set! (gp-lookup V *S*)
                   (cons Y (u-var!)))
           (gp-swap-to-a)
           (CC S C))))))
  (f L))

(define *tag->budy* #f)
(define (budy-it X)
  (umatch (#:mode - #:status *S*) (X)
    (('or  . _)  X)
    (('and . _)  X)
    (('not _  )  X)
    (_           
     `(budy ,X ,(gp-budy X)))))
                                                
          
;; recursive budies algorithm
(define (unify-rec S Pr CC F X Y)
  (define (memberR R A Z)
    (define (f Z)
      ;(pp (my-scm  `(memberR ,Z) S))
      (umatch (#:mode - #:status S) (Z)
        ((R2 A2 . L) 
         (if (equal-match S `(,R ,A) `(,R2 ,A2))
             #t
             (f L)))
        
        (V           
         (begin
           (gp-swap-to-b)
           (gp-set! (gp-lookup V *S*)
                    (u-cons (gp-cp R) 
                            (u-cons (gp-cp A) 
                                    (u-var!)))
                    *S*)
           (gp-swap-to-a)
           ;(pp (my-scm `(memberR ,R ,A -> ,Z) *S*))
           #f))))
    ;(pp (my-scm `(memberR ,R ,A -> ,Z) *S*))
    (f Z))
  
  (define (budy S X Zx)
    (umatch (#:mode - #:status S) ((budy-it Y))
      ((budy ('rec R A C) Zr)
       (begin
         ;;(pp (my-scm `(rec: ,R ,A budy: ,X ,Zx)))
         (let ((S (gp-set! Y C S)))
           (if (memberR R A Zx) 
               (CC S Pr)
               (add-rec 
                S R A C *u2*
                (lambda (S C)
                  (unify-rec S Pr CC F X C)))))))
      
      (('budy Y Zy)
       (F S Pr CC X Y))
      
      (_ 
       (F S Pr CC X Y))))
  
  (define (rec S R Zr A C)
    ;(pp (my-scm `(rec ,S ,R ,Zr ,A ,C) S))
    (umatch (#:mode - #:status S) ((budy-it Y))
      (('budy ('rec Ry Ay Cy) Zry)
       (begin
         ;(pp (my-scm `(rec: ,R ,A budyrec: (,Ry, ,Ay ,Cy) ,Zry) S))
         (let ((S (gp-set! Y Cy S)))         
           (cond
            ((memberR R A Zry)
             (CC S Pr))
            ((memberR Ry Ay Zr)
             (CC S Pr))
            (else
             (add-rec 
              S R A C *u1*
              (lambda (S C)
                (add-rec 
                 S Ry Ay Cy *u2*
                 (lambda (S Cy)
                   (unify-rec S Pr CC F 
                              C Cy))))))))))

      (('budy Y Zy)
       (begin
         ;(pp (my-scm `(rec: ,R ,A budy: ,Y ,Zy) S))
         (if (memberR R A Zy)
             (CC Pr)
             (add-rec 
              S R A C *u1*
              (lambda (S C)
                (unify-rec S Pr CC F C Y))))))
            
      ((H . L) (F S Pr CC X Y))))

  (pr 4 'unify-rec X Y)

  (umatch (#:mode - #:status S) ((budy-it X))
    (('budy ('rec R A C) Zr)
     (let ((S (gp-set! X C S)))
       (rec S R Zr A C)))

    (('budy X Zx)
     (budy S X Zx))
            
    (_ (F S Pr CC X Y))))
         




(define (clean-lambda x)
  (define (f x)
    (umatch (#:mode - #:status *S*) (x)
      (('lambda x) (let ((x (u-scm x)))
                     (if (procedure? x)       
                         (let-values (((Args Res) (x)))
                           (f `(lambda ,Args ,Res)))
                         x)))
      ((x     . l) (cons (f x) (f l)))
      (x           x)))
  (f x))

(define *branch-prompt* (gp-make-fluid *current-stack*))

(define (must-success S Pr CC F Error)
  (umatch  (#:tag fail #:status S) ()
    ( (F S fail (lambda (S Pr3) (CC S Pr))))
    ( (Error) )))

(define *branch-list* '())
(define (branch S Pr CC F1 F2)
  ;;br-id get the values of the curent undo list
  (let ((br-id (if (null? *branch-list*)
                   1
                   (let ((r (car *branch-list*)))
                     (set! *branch-list* (cdr *branch-list*))
                     r))))
    (gp-dynwind
        (lambda () #f)
        (lambda ()
          (match br-id
            (2  (F2 S Pr CC))
            (1  (umatch  (#:tag fail #:status S) ()
                  ( (with-gp-fluids S ((*branch-prompt* 
                                        (lambda () (fail))))
                        (F1 S Pr CC)))
                  ( (begin 
                      (set! br-id 2)
                      (set! *branch-list* '())
                      (F2 S Pr CC)))))))
        (lambda ()
          (set! *branch-list* (cons br-id *branch-list*)))
        S)))

(<define> (branch2 F1 F2)
  (<or> (F1) (F2)))

(define (sym->var Code)
  (define *symbols* '())
  (define *vars*    #f)

  (define (find-var X)
    (match X
      ((X . L)
       (begin 
         (find-var X)
         (find-var L)))

      ((? solve-var? X) 
       (if (not (member X *symbols*))
           (begin
             (set! *symbols* (cons X        *symbols*))
             (set! *vars*    (cons (u-var!) *vars*)))))
           
      (_          'ok)))
    
  (define (get-var X S Z)
    (match S
      (((? (?? X)) . L) 
       (car Z))
      (( _ . L) (get-var X L (cdr Z)))
      (()       (error "did not find var!"))))

  (define (subst X)
    (match X
      ((X . L)           (cons (subst X) (subst L)))
      ((? solve-var? X)  (get-var X *symbols* *vars*))
      (X                 X)))
  
  (find-var Code)
  (subst    Code))


(define (code->solver Code)
  (define *symbols* '())
  (define *vars*    #f)

  (define (find-var X)
    (match X
      ((X . L)
       (begin 
         (find-var X)
         (find-var L)))

      ((? solve-var? X) 
       (if (not (member X *symbols*))
           (set! *symbols* (cons X *symbols*))))
      
      (_          'ok)))
  
  (define (get-var X S Z)
    (match S
      (((? (?? X)) . L) 
       (car Z))
      (( _ . L) (get-var X L (cdr Z)))
      (()       (error "did not find var!"))))

  (define (subst X)
    (match X
      ((X . L)           (cons (subst X) (subst L)))
      ((? solve-var? X)  (get-var X *symbols* *vars*))
      (X                 X)))

  (set! *branch-list* '())
  
  ;; clear prolog variable stacks and setup environment

  ;; A bank
  (gp-clear *S*)
  (set! *produced-var* (u-var!))

  ;; B bank
  (gp-swap-to-b)
  (gp-clear *S*)
  (set! *u1*           (u-var!))
  (set! *u2*           (u-var!))
  (gp-swap-to-a)

  ;; enter realations into the database to be able to work with parametrized
  ;; and subtypes

  (prepare-subtype)
  (let ((A    (check Code 'Tres))
        (ret '()))
    (find-var A)
    (set! *vars* (map (lambda x (u-var!)) *symbols*))

    (let ((ret '()) (i 0))
      (umatch (#:tag fail #:status *S*) ()
        ((begin
           (gp-fluid-set! *branch-prompt* 
                          (lambda () (fail))
                          *S*)
           (<eval> *S* (r)
             ;;code
             (type-solver 1 (add-var-points (subst A)))

             ;;fail
             (lambda () 
               (format
                #t 
                "to high complexity, check not able to finish~%")
               (cons i ret))

             ;;cc
             (lambda (S Pr)  
               (if (< i 10000)
                   (begin
                     (set! i   (+ i 1))
                     (set! ret (cons
                                (gp->scm 
                                 (my-scm
                                  (clean-lambda
                                   (get-var 'Tres *symbols* *vars*)) S)
                                 S)
                                ret))

                     (u-abort (gp-fluid-ref *branch-prompt*)))
                   (cons i ret))))))
        
        ((cons i ret))))))
    

(define (pp X)       (begin (pretty-print X) X))
(define union  (lambda (x y) (lset-union        eq? x y)))

;;So this is the initial solver for the code taking care of outer boolean logic
;;and translating it via a backtracking mechanism.

(<define> (type-solver E X)
  (<dyn> (when *type-trace* (pp (my-scm `(type-solver       ,E ,X) S)))
         (when *type-trace* (pp (my-scm `(leave-type-solver ,E ,X) S))))
  (<match> (#:mode -) (E X)
    (-1 ['and     X Y]  (<cut> (branch (</.> (type-solver -1 X))
                                       (</.> (type-solver -1 Y)))))
    ( 1 ['and     X Y]  (<cut> (<and> (type-solver  1 X) 
                                      (type-solver  1 Y))))

    (-1 ['or      X Y]  (<cut> (<and> (type-solver -1 X) 
                                      (type-solver -1 Y))))
    
    ( 1 ['or      X Y]  (<cut> (branch (</.> (type-solver  1 X)) 
                                       (</.> (type-solver  1 Y)))))
    (-1 ['not     X  ]  (<cut> (type-solver  1 X)))
    ( 1 ['not     X  ]  (<cut> (type-solver -1 X)))
    ( E ['=       X Y]  (<cut> (my-unify E X Y)))
    (-1 [X       .  Y]  (<cut> (branch  (</.> (type-solver -1 X))
                                        (</.> (type-solver -1 Y)))))
    ( 1 [X       .  Y]  (<cut> (<and> (type-solver  1 X) 
                                      (type-solver  1 Y))))
    ( E [X]             (<cut> (type-solver E X)))
    ( E []              (<cut> <cc>))
    ( _ _               (<code> (error "type error")))))


;;This technique mans that a unification only matches one time and not
;;Many

(define (the-lambda S Pr CC G F A R)
  (<define> (z A AA B BB)
    (<and> (G A AA)
           (G R BB)))
  (let-values (((Arg Res) ((gp->scm F S))))
    (z S Pr CC
       A (add-var-points Arg)
       R (add-var-points Res))))

(<define> (my-unify E X Y)
  (must-success   
   (</.> (the-unify E X Y))
   (lambda ()
     (format #t "could not unify:~%")
     (pp (my-scm `(= (,E) ,X ,Y) *S*))
     (error "type-error"))))

(<define> (<the-lam> E X Y)
    (<match> (#:mode -) (X Y)
      ([F]          [A R]     
       (the-lambda (<lambda> (A B) (the-unify E A B)) F A R))
      ([A R]        [F]       
       (the-lambda (<lambda> (A B) (the-unify E A B)) F A R))
      ([F]          [G]  
       (<code> (error  "lambda f = lambda g")))
      (_ _ (error "the-lam match error"))))


(define *validated* (gp-make-fluid *current-stack*))

(gp-fluid-set! *validated* #f *S*)

(define (validating S Pr CC F E X Y)
  (with-gp-fluids S ((*validated* #t))
                  (F S Pr 
                     (lambda (S Pr)
                       (with-gp-fluids *S* ((*validated* #f))
                                       (CC S Pr)))
                     E 1 X Y)))


(<define> (the-unify E X Y)
  (<dyn> (pr 0 'the-unify E X Y) (pr 0 'leave-the-unify E X Y))
  (<match> (#:mode -) (X Y)
    (['lambda X]          ['lambda Y]       (<cut> (<the-lam> E X Y)))     
    
    (['res ['lambda F]]  ['def ['lambda A R]]
     (<cut> (the-lambda (<lambda> (A B) 
                          (%a=r%1 E 1 A B))
                        F A R)))

    (['validated A]       ['res B]          (<cut> 
                                             (validating %r=a%1 E A B)))
    (['res B]             ['validated A]    (<cut>
                                             (validating %a=r%1 E B A)))

    (['arg A]             ['res B]          (<cut> (%a=r%1 E 1 A B)))
    (['res A]             ['arg B]          (<cut> (%r=a%1 E 1 A B)))
    
    (['or . L]            ['def A]          (defor E `(def ,A) L))
    (['def A]             ['arg B]          (<cut> (<=> A B)))
    (['def A]             ['res B]          (<cut> (<=> A B)))
    (['def A]             B                 (<cut> (<=> A B)))

    (['arg A]             ['def B]          (<cut> (<=> A B)))
    (['res A]             ['def B]          (<cut> (<=> A B)))
    (A                    ['def B]          (<cut> (<=> A B)))

    (['res A]             B                 (<cut> (%r=a%1 E 1 A B)))
    (B                    ['res A]          (<cut> (%r=a%1 E 1 A B)))

    (['arg A]             B                 (<cut> (%r=a%1 E 1 B A)))
    (B                    ['arg A]          (<cut> (%r=a%1 E 1 B A)))

    (A                    B                 (<cut> (<=> A B)))
    (A                    B                 (<cut> (%r=a%1 E 1 A B)))))

(define (%r=a%1 S Cut CC NotX NotY X Y) (%a=r%1 S Cut CC NotY NotX Y X))
(<define> (defor E Def OR)
    (<match> (#:mode -) (OR)
      ((X . L)  (the-unify E Def X))
      ((_ . L)  (<cut> (defor E Def L)))
      (_        <fail>)))
  
  
;;The B stack bank is alive for the entire unifying construction
;;After it will backtrack and reset during unify temporary settings
(define (store-vars X)
  (define vars '())
  (define (f X)
    (umatch (#:mode - #:status *S*) (X)
            ((X . L) 
             (begin (f X) (f L)))
            (X 
             (let ((X (gp-lookup X *S*)))
               (if (gp-var? X *S*)
                   (if (member X vars)
                       #t
                       (set! vars (cons X vars)))
                   #t)))))
  (f X)
  (set! *res-vars* vars))

(define (%a=r%1 S Pr CC Ex Ey X Y)
  (store-vars Y)
  (u-set! *produced-var* (gp-var! S))
  (let ((Fr (b-frame)))
    (b-page S Pr CC Fr
            (lambda (S Pr CC)
              (%a=r% S Pr (lambda (S Pr)
                            (b-unwind Fr)
                            (CC S Pr))
                     Ex Ey X Y)) 1)))

;;taking care of inner boolean logic
(<define> (%a=r% NotX NotY X Y)
  (<dyn> (pr 2 'a=r NotX NotY X Y) (pr 2 'leav-a=r NotX NotY X Y))
  (<match> (#:mode -) (X Y)
    (['lambda F]          ['lambda A R]     
     (the-lambda (<lambda> (A B) 
                   (%r=a%1 NotX NotY A B)) 
                 F A R))
    
    (['lambda A R]        ['lambda F]       
     (the-lambda (<lambda> (A B) 
                   (%a=r%1 NotX NotY A B)) 
                 F A R))

    (['lambda F]          ['lambda G]       
     (<code> (error  "lambda f = lambda g")))

    ((? var? X)        Y
     (<cut> (unify%a=r% NotX NotY X Y)))


    (['not ['not   X]] Y        
     (<cut> (%a=r% NotX NotY X Y)))

    (['not ['or  . L]] Y        
     (<cut> (%and% %a=r% (- NotX) NotY L Y)))

    (['not (and A ['and . L])] Y        
     (<cut>  (%or%  %a=r% A (- NotX) NotY L Y)))
                     
    ((and A ['or . L])        Y      
     (<cut> (%or% %a=r% A NotX NotY L Y)))

    (['and . L]        Y        
     (<cut> (%and% %a=r% NotX NotY  L Y)))

    (Y ['not ['not   X]]        
       (<cut> (%a=r% NotX NotY Y X)))

    (Y ['not ['and  . L]]        
       (<cut> (%try% %r=a%1 (- NotY) NotX  L Y)))

    (Y ['not (and A ['or . L])]        
       (<cut> (%or%  %r=a%1 A (- NotY) NotX L Y)))

    (Y              ['or  . L]
        (<cut> (%try% %r=a%1 NotY NotX  L Y)) )
    
    (Y              (and A ['and . L])
        (<cut> (%or%  %r=a%1 A NotY NotX L Y)))

    ((? var? X)        Y
     (unify%a=r% NotX NotY X Y))
             
    (X             (? var? Y)
        (<cut> (unify%a=r% NotX NotY X Y)))

    (X                 Y        
      (<cut> (unify%a=r% NotX NotY X Y)))

    (_ _ <fail>)))

;; patching in the ability of subtyping
(define (unify%a=r% s pr cc ex ey x y)
  (unify-rec s pr cc 
             (lambda (s pr cc x y)
               (<subtype-unify-ar> 
                s pr cc
                unify%a=r%0 
                ex ey x y))
             x y))

(define (res-var S Pr CC Ex Ey X Y)
  (if (gp-fluid-ref *validated* S) 
      (let ((S (gp-set! Y X S)))
        (CC S Pr))
      (u-abort Pr)))

;; unification and list destruction
(<define> (unify%a=r%0 EX EY X Y)
  (<dyn> (pr 8 'unify-a=r EX EY X Y) (pr 8 'leave-unify-a=r EX EY X Y))
  (<match> (#:mode -) (EX EY X Y)
       (_ _  ('rec . L) _ 
          (<cut> (unify%a=r% EX EY (cons 'rec  L) Y)))
       (_ _   _ ('rec . L)  
          (<cut> (unify%a=r% EX EY X (cons 'rec  L))))

       ;; lambda expansion
       (NotX NotY ['lambda F]           ['lambda A R]     
        (<cut> 
         (the-lambda (<lambda> (A B) 
                       (%a=r% NotX NotY A B)) 
                     F A R)))

       (NotX NotY ['lambda A R]        ['lambda F]       
        (<cut>
         (the-lambda (<lambda> (A B) 
                       (%a=r% NotX NotY A B)) 
                     F A R)))

       (_ _ ['lambda F]          ['lambda G]       
          (<code> (error "lambda f = lambda g")))
       
       ( E1 E2 ['lambda A1 R1] ['lambda A2 R2]
            (<cut>
             (<and> 
              (swap-u1-u2)
              (unify%a=r%0 E2 E1 A2 A1)
              (swap-u1-u2)
              (%a=r% E1 E2 R1 R2))))

        ;; list destruction
       ( 1  1 [A  . B1]       [A  . B2]    (<cut> (%arg%  %a=r% 1 1 B1 B2)))
       ( 1  1 [_  . _ ]       [_  . _ ]    (<cut> <fail>))
       ( _ -1 [A  . B1]       [A  . B2]    (<cut> <fail>))
       (-1  1 [A  . B1]       [A  . B2]    (<cut> (%arg%  %a=r% -1 1 B1 B2)))
       (-1  1 [A1 . B1]       [A2 . B2]    (<cut> (<cc>)))

       ;;unification
       ( 1  1 (? var? X)     (? var? Y)  (<cut> (<x=> X Y)))
       (-1  1 (? var? X)     (? var? Y)  (<cut> (<x=> X ['not Y])))
       ( 1 -1 (? var? X)     (? var? Y)  (<cut> (<x=> X ['not Y])))
       (-1 -1 (? var? X)     (? var? Y)  (<cut> (<x=> X Y)))
       (Ex Ey  X             (? var? Y)  (<cut> (res-var Ex Ey X Y)))
        
       ( 1  1 (? var? X)     Y           (<cut> (<x=> X  Y)))
       (-1 -1 (? var? X)     Y           (<cut> (<x=> X  Y)))
       (-1  1 (? var? X)     Y           (<cut> (<x=> X  ['not Y])))
       ( 1 -1 (? var? X)     Y           (<cut> (<x=> X  ['not Y])))

       ;; atom treatment
       ( 1  1 A               A          (<cut> <cc>  ))
       (_  -1 _               _          (<cut> <fail>))
       (-1  1 A               A          (<cut> <fail>))
       (-1  1 A               B          (<cut> <fail>))
       (_ _ _ _ (<cut> <fail>))))

(define *produced-var* #f)
(define (qmember X L)
  (umatch (#:name 'qmember #:mode - #:status *S*) (L)
          ((,X . _) #t)
          ((_  . L) (qmember X L))
          (_        #f)))

(define (qappend X L)
  (umatch (#:name 'qappend #:mode - #:status *S*) (X)
          ((_  . U) (qappend U L))
          (U        (begin
                      (u-set! U L)
                      #f))))


(define (<x=> S Pr CC X Y)
  (define (f Y L CC)
    (umatch (#:name '<x=> #:mode - #:status S) (Y)
            ((X . U) (f X L (lambda (L) (f U L CC))))
            (X       (let ((X (gp-lookup X S)))
                       (if (gp-var? X S) 
                           (if (or (qmember X L) (qmember X *produced-var*))
                               (CC L)
                               (CC (cons X L)))
                           (CC L))))))

  ;;(pp (my-scm `(x= ,*produced-var*)))
  (let ((X (gp-lookup X S))
        (Y (gp-lookup Y S)))
    (if (gp-var? X S)
        (if (qmember X *produced-var*)
            (if (eq? X Y)
                (begin #;(pp 'matched)   (CC S Pr))
                (begin #;(pk 'no-match)  (u-abort Pr)))
            (f Y (gp-var! S)
               (lambda (L)
                 (qappend *produced-var* L)
                 (let ((S (if (not (eq? X Y)) (gp-set! X Y S) S)))
                   (CC S Pr)))))
        (error "<x=> should be called with X a variable"))))

(<define> (%and% F A B X Z)
   (<match> () (X)
            ([X]     (<cut> (<funcall> F A B X Z)))
            ([X   Y] (<cut> (<and> (<funcall> F A B X Z) (<funcall> F A B Y Z))))
            ([X . L] (<cut> (<and> (<funcall> F A B X Z) (%and% F A B L Z))))
            (_       (error "match error in %and%"))))

(<define> (%try% F A B X Z)
   (<match> () (X)
            ([X]     (<cut> (<funcall> F A B X Z)))
            ([X   Y] (<cut> (branch2
                             (</.> (<funcall> F A B X Z))
                             (</.> (<funcall> F A B Y Z)))))
            ([X . L] (<cut> (branch2
                             (</.> (<funcall> F A B X Z)) 
                             (</.>  (%try% F A B L Z)))))
            (_       (error "match error in %try%"))))


(define (parenthood S Pr CC U X)
  (let ((S (if (gp-fluid-ref *validated* S)
               (gp-set! U X S)
               S)))
    (CC S Pr)))
      

;; Unwinding both A bank and B bank in or construction in order to backtrack
;; B bank = during unifying backtracking.

(<define> (%or% F U A B X Z)
    (<let> ((Fr (b-frame)))
         (b-page Fr
             (</.> (%or%0 F U A B X Z Fr)) 4)))
           

(<define> (%or%0 F U A B X Z Fr)
  (<match> () (X)
    ([X]       (<cut> (<and>
                       (<when> (begin (b-unwind Fr) #t))
                       (<funcall> F A B X Z)
                       (parenthood U X))))

    ([X   Y]   (<cut> (<or> (<and>
                             (<when> (begin (b-unwind Fr) #t))
                             (<funcall> F A B X Z)
                             (parenthood U X))
                            (<and> 
                             (<when> (begin (b-unwind Fr) #t))
                             (<funcall> F A B Y Z)
                             (parenthood U Y)))))

    ([X . L]   (<cut> (<or> (<and>
                             (<when> (begin (b-unwind Fr) #t))
                             (<funcall> F A B X Z)
                             (parenthood U X))
                            (%or%0 F U A B L Z Fr))))
    (_ (error "match error in %or%"))))

           
(<define> (%arg% F EX EY X Y)
    ;;(<pp> `(arg ,EX ,EY ,X ,Y))
   (<match> (#:mode -) (EX EY X Y)
          ( _  _ []      []          <cc>)

          ( 1  1 [X]     [A    ]    (<cut> (<funcall> F 1 1 X A)))
          ( 1  1 [X . Y] [A . B]    (<cut> 
                                     (<and> 
                                      (<funcall> F 1 1 X A) 
                                      (%arg% F 1 1 Y B))))

          (-1 -1 [X]     [A    ]    (<cut> (<funcall> F -1 -1 X A)))
          (-1 -1 [X . Y] [A . B]    (<cut> (<and> (<funcall> F -1 -1 X A) 
                                           (%arg% F -1 -1 Y B))))

          ( K  L [X]     [A    ]    (<cut> (<funcall> F K L X A)))
          ( K  L [X . Y] [A . B]    (<cut> (<or> (<funcall> F K L X A) 
                                                 (%arg% F K L Y B))))

          ( A B X Y (<code> (error "match error in %arg%")))))

